# my nix config
these are configs i use for nixos.

they might not work on your system because of some hardware stuff. if so remove this in `nixos/configuration.nix`:

```nix
fileSystems."/mnt/SteamGames" = {
    device = "/dev/sdb1"; # "91fb03f7-4536-4c32-962a-8446d4fb734e";
    label = "SteamGames";
    fsType = "ext4";
    options = [
        "defaults"
            "nofail"
    ];
};
```

# install
run the following:

```bash
git clone https://gitlab.com/thatonepuggo/nix-cfg ~/.config/nixos && cd ~/.config/nixos
sudo nixos-rebuild switch .#desktop
```

you might need to add a new system config in flake.nix instead of `desktop`.

where you can add a system:

```nix
in {
    nixosConfigurations = {
# Now, defining a new system can be done in one line
#                                             Architecture   Hostname
        desktop = mkSystem inputs.nixpkgs "x86_64-linux" "nixos";
# below here
        myConfig = mkSystem inputs.nixpkgs "architecture" "hostname";
    };
};
```

once you have modified it to your needs, run `sudo nixos-rebuild switch .#<the name of your configuration>`.

in the case of the `myConfig` line, you would use `sudo nixos-rebuild switch .#myConfig`.

but do note that cuz im a noob at nixos, i can't really help with problems.

# credits
notusknot: i used his config as a base sometimes: https://github.com/notusknot/dotfiles-nix
dt: i used his wallpapers (the ones that are numbered are his): https://gitlab.com/dwt1/wallpapers
unsplash: they have free stock images for some wallpapers i use (https://unsplash.com/)
