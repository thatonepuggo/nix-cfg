# i have not written flakes before so i copied from notusknot a bit: https://github.com/notusknot/dotfiles-nix/blob/main/flake.nix
{
  description = "NixOS configuration";

  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/nixos-unstable";

    home-manager = {
      url = "github:nix-community/home-manager";
      inputs.nixpkgs.follows = "nixpkgs";
    };

    nur = {
      url = "github:nix-community/NUR";
      inputs.nixpkgs.follows = "nixpkgs";
    };
  };

  outputs = { home-manager, nixpkgs, nur, ... }@inputs:
    let
      system = "x86_64-linux";
      pkgs = inputs.nixpkgs.legacyPackages.x86_64-linux;
      lib = nixpkgs.lib;

      mkSystem = pkgs: system: hostname:
      pkgs.lib.nixosSystem {
        system = system;
        modules = [
          { networking.hostName = hostname; }
# General configuration (users, networking, sound, etc)
          ./nixos/configuration.nix
          ./nixos/hardware-configuration.nix

           home-manager.nixosModules.home-manager
           {
              home-manager = {
                useUserPackages = true;
                useGlobalPkgs = true;
                extraSpecialArgs = { inherit inputs; };
# Home manager config (configures programs like firefox, zsh, eww, etc)
                users.pug = ./home-manager/home.nix;
              };
              nixpkgs.overlays = [
                nur.overlay
#                (import ./overlays)
              ];
           }
        ];
          specialArgs = { inherit inputs; };
        };

        in {
          nixosConfigurations = {
# Now, defining a new system can be done in one line
#                                             Architecture   Hostname
            desktop = mkSystem inputs.nixpkgs "x86_64-linux" "nixos";
          };
        };

}
