{ config, pkgs, ... }:
{ 
  networking.hostName = "nixos"; # does this belong here?

  users.users.pug = {
    isNormalUser = true;
    description = "Ben";
    extraGroups = [ "networkmanager" "wheel" ];
    packages = with pkgs; [
        kate
        thunderbird
        brave
        firefox
    ];
  };

  programs.zsh = {
    enable = true;
    autosuggestions.enable = true;
    syntaxHighlighting.enable = true;
  };
  users.defaultUserShell = pkgs.zsh;
}
