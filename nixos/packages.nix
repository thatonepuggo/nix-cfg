{ config, pkgs, ... }:
{ 
# Allow unfree packages
  nixpkgs.config.allowUnfree = true;
  programs.steam.enable = true;

  environment.systemPackages = with pkgs; [
# basic tools
      exa ripgrep fd bat
      wget curl git file 
      lsd
# daemons
      lxde.lxsession nitrogen
# shells
      zsh starship
# pkg managers
      flatpak
# editors
      helix # neovim is not here, it's in home-manager/home.nix
      slade # not a text editor, a doom wad editor.
# chat
      discord
# games
      steam gzdoom doomrunner
      gamemode
# languages
      nodejs python3
# eye candy
      nerdfonts apple-cursor
# WMs
      bspwm sxhkd xorg.xinit
# record
      obs-studio
# screenshot
      grim slurp flameshot
# video editing
      kdenlive mediainfo ffmpeg-full frei0r
    ];

  # overlays
  nixpkgs.overlays =
    let
    discordOverlay = self: super: {
      discord = super.discord.override { withTTS = true; };
    };
  in
    [ discordOverlay ];
}
