{ config, ... }:
{
  fileSystems."/mnt/SteamGames" = {
    device = "/dev/sdb1"; # "91fb03f7-4536-4c32-962a-8446d4fb734e";
    label = "SteamGames";
    fsType = "ext4";
    options = [
        "defaults"
        "nofail"
    ];
  };
}
