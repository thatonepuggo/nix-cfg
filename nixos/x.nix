{ config, pkgs, ... }:
{
  services.xserver = {
    enable = true;
    displayManager.sddm.enable = true;
    desktopManager.plasma5.enable = true;
    desktopManager.mate.enable = true;
    windowManager.bspwm.enable = true;
    # configure keymap in X11
    layout = "us";
    xkbVariant = "";
  };
  # Enable touchpad support (enabled default in most desktopManager).
  # services.xserver.libinput.enable = true;
}
