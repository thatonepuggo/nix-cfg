{ lib, config, pkgs, ... }: {
  home.username = "pug";
  home.homeDirectory = "/home/pug";

  imports = [
    ./config/nvim/nvim.nix
    ./config/zsh.nix
    ./config/starship.nix
    ./config/obs-studio.nix
    ./config/bspwm.nix
    ./config/sxhkd.nix
    ./config/rofi.nix
    ./config/polybar.nix
  ];
  home.packages = with pkgs; [
    htop
    wezterm
    btop
    mpv
    vlc
    neofetch
    rofi
  ];
  home.stateVersion = "23.05";
  
  home.file.".icons/default".source = "${pkgs.apple-cursor}/share/icons/macOS-BigSur";
  gtk = {
    enable = true;
    cursorTheme = {
      package = pkgs.apple-cursor;
      name = "macOS-BigSur";
    };
    theme = {
      name = "Dracula";
      package = pkgs.dracula-theme;
    };
  };

  programs.home-manager.enable = true;
}
