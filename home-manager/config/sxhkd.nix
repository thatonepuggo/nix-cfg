{ config, lib, pkgs, ... }: {
  config.services.sxhkd = {
    enable = true;
    keybindings = {
      ### wm independent
      "super + r" = "rofi -show drun"; # open rofi
      "super + Return" = "wezterm"; # spawn a terminal
      "super + Print" = "flameshot gui"; # take a screenshot using flameshot
      ### bspwm specific
      "super + alt + {s,r}" = "{pkill -USR1 -x sxhkd,bspc wm -r}"; # super + alt + s: restart sxhkd. super + alt + r: restart bspwm.
      "super + alt + q" = "bspc quit"; # quit bspwm
      "super + {c,k}" = "bspc node -{c,k}";
      "super + m" = "bspc desktop -l next"; # alternate between the tiled and monocle layout
      "super + y" = "bspc node newest.marked.local -n newest.!automatic.local"; # send the newest marked node to the newest preselected node
      "super + g" = "bspc node -s biggest.window"; # swap the current node and the biggest window
      # state/flags
      "super + {t,shift + t,s,f}" = "bspc node -t {tiled,pseudo_tiled,floating,fullscreen}"; # set the window state
      "super + ctrl + {m,x,y,z}" = "bspc node -g {marked,locked,sticky,private}"; # set the node flags
      # focus/swap
      "super + {_,shift + }{h,j,k,l}" = "bspc node -{f,s} {west,south,north,east}"; # focus the node in the given direction
      "super + {p,b,comma,period}" = "bspc node -f @{parent,brother,first,second}"; # focus the node for the given path jump
      "super + bracket{left,right}" = "bspc desktop -f {prev,next}.local"; # focus the next/previous desktop in the current monitor
      "super + {_,shift + }{1-5}" = "bspc {desktop -f,node -d} '^{1-5}'"; # super + 1-5: move screen to desktop. super + shift + 1-5: move window to desktop
      # preselect
      "super + ctrl + {h,j,k,l}" = "bspc node -p {west,south,north,east}"; # preselect the direction
      "super + ctrl + {1-9}" = "bspc node -o 0.{1-9}"; # preselect the ratio
      "super + ctrl + space" = "bspc node -p cancel"; # cancel the preselection for the focused node
      "super + ctrl + shift + space" = "bspc query -N -d | xargs -I id -n 1 bspc node id -p cancel"; # cancel the preselection for the focused desktop
      # move/resize
      "super + alt + {h,j,k,l}" = "bspc node -z {left -20 0,bottom 0 20,top 0 -20,right 20 0}"; # expand a window by moving one of its sides outward
      "super + alt + shift + {h,j,k,l}" = "bspc node -z {right -20 0,top 0 20,bottom 0 -20,left 20 0}"; # contract a window by moving one of its sides inwards
      "super + {Left,Down,Up,Right}" = "bspc node -v {-20 0,0 20,0 -20,20 0}"; # move a floating window
    };
  };
}
