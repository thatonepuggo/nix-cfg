{ lib, config, pkgs, ... }: {
  config.xsession.windowManager.bspwm = {
    enable = true;
    startupPrograms = [
      "sxkhd"
      "nitrogen --restore"
      "lxsession"
      "polybar"
    ];
    settings = {
      gapless_monocle = true;

      window_gap = 0;

      split_ratio = 0.52;
      border_width = 0;

      click_to_focus = false;
    };
    monitors = {
      HDMI-2 = [
        "I" "II" "III" "IV" "V"
      ];
      DVI-1 = [
        "I" "II" "III" "IV" "V"
      ];
    };

  };

}
