{ lib, config, pkgs, ... }: {
  home.packages = with pkgs; [
    starship zsh-autosuggestions zsh-syntax-highlighting
  ];
  programs.zsh = {
    enable = true;

    shellAliases = {
        ls = "lsd";
        ll = "ls -l";
        la = "ls -la";

        res = ". $HOME/.zshrc";

        # nix tools
        rebuild = "cd $HOME/.config/nixos; sudo nixos-rebuild switch --flake .#desktop --fast";
        gclean = "nix-env --delete-generations old && nix-store --gc";

        vim = "nvim";
        vi = "nvim";
        v = "nvim";
        vc = "nvim $HOME/.config/home-manager/home.nix";
	
        vide = "neovide";
    };

    history = {
        size = 10000;
        path = "${config.xdg.dataHome}/zsh/history";
    };
  };
}
