{ lib, config, pkgs, ... }: {
#  home.packages = with pkgs; [
#    neovim neovide
#  ];
  programs.neovim = {
    enable = true;
    viAlias = true;
    vimAlias = true;

    plugins = with pkgs.vimPlugins; [
# syntax highlighting
        rust-vim
        nvim-treesitter-parsers.sxhkdrc
        nvim-treesitter-parsers.zig
        nvim-treesitter-parsers.rasi
        nvim-treesitter.withAllGrammars
        vim-nix
# lualine
        lualine-nvim
        vim-devicons
# theme
        gruvbox
        catppuccin-nvim
        rainbow
# fzf
        nvim-fzf
# emmet
        emmet-vim
    ];

    extraConfig = ''
      luafile ${config.xdg.configHome}/nixos/home-manager/config/nvim/config.lua
      '';
  };
}
