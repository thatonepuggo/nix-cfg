local opt = vim.opt
local g = vim.g
local autocmd = vim.api.nvim_create_autocmd

vim.cmd [[
    colorscheme catppuccin-macchiato

    set nowrap
    set nobackup
    set nowritebackup
    set noerrorbells
    set noswapfile
]]

g.mapleader = ' '

-- numbers
opt.number = true
opt.relativenumber = true

-- system clipboard
opt.clipboard = "unnamedplus"

-- mouse
opt.mouse = "a"

-- indent
opt.smartindent = true
opt.autoindent = true
opt.tabstop = 4
opt.shiftwidth = 4
opt.expandtab = true

autocmd(
    "FileType",
    { 
        pattern = "nix", command = [[
            set tabstop=2
            set shiftwidth=2
        ]] 
    }
)
