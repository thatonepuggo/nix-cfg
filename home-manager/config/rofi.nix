{ config, lib, pkgs, ... }: {
  programs.rofi = {
    extraConfig = ''
@theme "dmenu"

configuration {
    modi: "drun,run,ssh,combi";
    combi-modi: "drun,run,ssh";
    show-icons: true;
    icon-theme: "Papirus";
}

* {
    background-color: #191A17;
    font: "Fira Code Nerd Font 15";
}

window {
    padding: 0px 0px;
}

prompt {
    padding: 0px 10px;
    background-color: #615A52;
}

element selected {
    background-color: #615A52;
}
      '';
  };
}
